#!/usr/bin/python

import sys

def usage():
    print '+------------------------------------------------------------+'
    print '|          Generador de fibras 0 1 0 para FSI                |'
    print '|                         | BSC |                            |'
#    print '|To check that the software has worked OK just execute this  |'
#    print '|script again to the modified file, and the message \"Some    |'
#    print '|lines were modified!\" shuldn\'t appear.                      |'
#    print '|Still not workin\'?! Write me a line:                        |'
#    print '|Alfonso Santiago - alfonso.santiago@bsc.es                  |'
    print '|Ultra beta version.                                         |'
    print '+------------------------------------------------------------+'
    sys.exit()


if(len(sys.argv)< 2):
    usage()
elif(sys.argv[1]=='-v'):
    usage()


f  = open(sys.argv[1], 'r')
fw = open(sys.argv[2], 'w')

FlagIni=False
FlagEnd=False


print 'Reading one file and writing the other.'        

for line in f:
    line = line.strip()
    if (line.startswith('MAT')):
        FlagIni=True
    elif(line.startswith('END')):
        FlagEnd=True
    else:
        line=line.split()
        if (int(line[1])==1):
            fw.write(line[0])
            fw.write(' 0.0')
            fw.write(' 0.0')
            fw.write(' 0.0')
            fw.write('\n')
        elif (int(line[1])==2 or int(line[1])==3):
            fw.write(line[0])
            fw.write(' 0.0')
            fw.write(' 1.0')
            fw.write(' 0.0')
            fw.write('\n')
        else:
            print 'El material no se ha tenido en cuenta! Modificar el codigo!'

f.close()        
fw.close()
print 'Bye! :)'

